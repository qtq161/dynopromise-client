require('dotenv').config()
const assert = require('assert')
const AWS = require('aws-sdk')

const dynamodb = require('../dist/db.js')

if (!process.env.TEST_AWS_REGION || !process.env.TEST_AWS_ENDPOINT) {
	throw new Error('Need region and endpoint for test')
}

const runConfigTests = () => {

	// Test that region and endpoint environment variables are picked up
	process.env.DYNOPROMISE_AWS_REGION = 'eu-west-1'
	process.env.DYNOPROMISE_DYNAMODB_ENDPOINT = 'http://doesnot4321exist:51234'
	const testEnvSettingsDb = dynamodb()
	assert.strictEqual(
		testEnvSettingsDb.getDocumentClient().options.region,
		'eu-west-1'
	)
	assert.strictEqual(
		testEnvSettingsDb.getDocumentClient().options.endpoint,
		'http://doesnot4321exist:51234'
	)
	console.log('Successfully tested environment variable configuration')

	// Test that variables based in override the environment variables
	const testEnvSettingsDb2 = dynamodb({
		region: 'us-east-1',
		endpoint: 'http://doesnot1234exist:54321'
	})
	assert.strictEqual(
		testEnvSettingsDb2.getDocumentClient().options.region,
		'us-east-1'
	)
	assert.strictEqual(
		testEnvSettingsDb2.getDocumentClient().options.endpoint,
		'http://doesnot1234exist:54321'
	)
	console.log('Successfully tested environment variable config override')
}

/**
 * Sets up a table which can be used for testing db functions.
 */
const setupDbTests = () => new Promise((resolve, reject) => {
	console.log('Starting tests')
	const fullDb = new AWS.DynamoDB({
		region: process.env.TEST_AWS_REGION,
		endpoint: process.env.TEST_AWS_ENDPOINT,
	})

	// Create a table to test on
	fullDb.createTable({
		TableName: 'dynopromiseClientTest89345',
		KeySchema: [
			{
				AttributeName: 'id',
				KeyType: 'HASH',
			}
		],
		AttributeDefinitions: [
			{
				AttributeName: 'id',
				AttributeType: 'S',
			}
		],
		ProvisionedThroughput: {
				ReadCapacityUnits: 5,
				WriteCapacityUnits: 5
		},
	}, err => {
		// Show any errors (except the error stating the table already exists)
		if (err && err.name !== 'ResourceInUseException') {
			reject(err)
		}
		console.log('Test table created')
		resolve()
	})
})

/**
 * Performs the tests using the promisified functions.
 */
const runDbTests = async () => {
	const db = dynamodb({
		region: process.env.TEST_AWS_REGION,
		endpoint: process.env.TEST_AWS_ENDPOINT,
	})

	// Test the put function
	const putResult = await db.put({
		TableName: 'dynopromiseClientTest89345',
		Item: {
			id: 'testABC',
			randomData: 'lorum ipsum',
		}
	})
	assert.ok(putResult)
	console.log('Successfully tested put method')

	// Test the get function
	const getResult = await db.get({
		TableName: 'dynopromiseClientTest89345',
		Key: { id: 'testABC' },
	})
	assert.ok(getResult.Item)
	console.log('Successfully tested get method')

	// Test the delete function
	const deleteResult = await db.delete({
		TableName: 'dynopromiseClientTest89345',
		Key: { id: 'testABC' }
	})
	assert.ok(deleteResult)
	console.log('Successfully tested delete method')

	// Test error handling
	let errorHit = false
	try {
		await db.get({
			TableName: 'dynopromiseClientTest89345DOES_NOT_EXIST',
			Key: { id: 'testABC' },
		})
	}
	catch {
		errorHit = true
	}
	assert.ok(errorHit)
	console.log('Successfully tested error handling')
}

/**
 * Deletes the database table set up for testing.
 */
const cleanupDbTests = () => new Promise((resolve, reject) => {
	const fullDb = new AWS.DynamoDB({
		region: process.env.TEST_AWS_REGION,
		endpoint: process.env.TEST_AWS_ENDPOINT,
	})

	fullDb.deleteTable(
		{
			TableName: 'dynopromiseClientTest89345',
		}, (err) => {
			if (err) {
				return reject(err)
			}
			console.log('Test table deleted')
			resolve()
		}
	)
})

/**
 * Runs the setup, tests and clean up.
 */
const runTests = async () => {
	try {
		await runConfigTests()
		await setupDbTests()
		await runDbTests()
		await cleanupDbTests()
		console.log('Testing complete\n')
	}
	catch (err) {
		console.error(err)
	}
}

runTests()
